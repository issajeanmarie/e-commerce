import React from 'react';
import './index.scss';

const MenuItem = ({ title, image, size }) => (    
    <div className={`${size} menu-item`}>
        <div className="background-img" style={{backgroundImage: `url(${image})`}}></div> 
        <div className="content">
            <p className="title">{title}</p>
            <span className="subtitle">Shop now</span>
        </div>
    </div>
)

export default MenuItem;