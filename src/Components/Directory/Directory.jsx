import React, { Component } from 'react';
import MenuItem from '../MenuItem';
import './index.scss';

export class Directory extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             items: [
                 {
                     title: 'HARTS',
                     img: '',
                     id: 0,
                     image: 'https://placeimg.com/640/480/tech/0'
                 },
                 {
                    title: 'SNEAKERS',
                    img: '',
                    id: 1,
                    image: 'https://placeimg.com/640/480/1'
                },
                {
                    title: 'JEANS',
                    img: '',
                    id: 3,
                    image: 'https://placeimg.com/640/480/3'
                },
                {
                    title: 'WOMENS',
                    img: '',
                    id: 2,
                    image: 'https://placeimg.com/640/480/2',
                    size: 'large'
                },
                {
                    title: 'MENS',
                    img: '',
                    id: 3,
                    image: 'https://placeimg.com/640/480/3',
                    size: 'large'
                }
             ]
        }
    }
    
    render() {
        return (
            <div className="directory-menu">
                {
                    this.state.items.map(item => (
                        <MenuItem key={item.id} size={item.size} title={item.title} image={item.image} />
                    ))
                }
            </div>
        )
    }
}

export default Directory
