import React from 'react';
import Directory from '../../Components/Directory';
import './index.scss';

function HomePage() {
    return (
        <div className="homepage">
            <div className="directory-menu">
                <Directory />
            </div>
        </div>
    )
}

export default HomePage
