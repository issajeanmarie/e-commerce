import './App.scss';
import HomePage from './Views/Homepage';

function App() {
  return (
    <HomePage />
  );
}

export default App;
